package com.teater.service;

import com.teater.model.Seats;
import com.teater.model.Seats;
import org.springframework.stereotype.Service;

@Service
public interface SeatsService {

    Seats addSeat(Seats seats);

    Seats getSeat(String seatsCode);
}
